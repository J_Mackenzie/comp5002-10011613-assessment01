﻿using System;

namespace demo_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome To The store"); //introducing customer
            Console.WriteLine("What is your name?"); // asking name
            Console.WriteLine("First name");

            var name1 = (Console.ReadLine()); // first name
            Console.WriteLine("Second name");
            var name2 = (Console.ReadLine()); // second name
            Console.Clear(); // clearing screen

            Console.WriteLine("Welcome"); // Welcoming user
            Console.WriteLine(name1); // first name
            Console.WriteLine(name2); // second name
            Console.WriteLine("*****************************************");
            Console.WriteLine("Please Type a 2 decimal number"); // asking for 2 decimal number
            var dec_num = Double.Parse(Console.ReadLine()); // input of number
            Console.WriteLine("Would you like to add another number Y/N"); // would either like to or not like to add another number
            var y = ""; //option for yes they want to add another number
                y = Console.ReadLine();
            if (y =="y")
            {
            Console.WriteLine("Please Type another 2 decimal place number"); // second number
            var dec_num1 = Double.Parse(Console.ReadLine()); 
            Console.WriteLine($"Your total comes to {dec_num+dec_num1}"); // total without gst
            Console.WriteLine($"Your total comes out to including gst {(dec_num1+dec_num)*1.15}"); // total with gst
            }
            else if(y =="n")
            {
                Console.WriteLine($"Your total comes to {dec_num}");
                Console.WriteLine($"Your total comes out to including gst {dec_num*1.15}"); // giving total with gst

            }
            Console.WriteLine("Thank you for shopping with us, please come again"); // thanking and farewelling customer
            
        }
    }
}
